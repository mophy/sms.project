-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 08, 2020 at 02:22 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.0.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sms`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'mohit', 'mohit');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(20) NOT NULL,
  `rollno` int(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `fname` varchar(30) NOT NULL,
  `city` varchar(50) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `course` varchar(11) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `rollno`, `name`, `fname`, `city`, `contact`, `email`, `course`, `image`) VALUES
(17, 1, 'Moh!t P@nd!t', 'Brijkishor', 'mathura', '9898978', 'mohitsharma131129@gmail.com', 'MCA', 'Jellyfish.jpg'),
(20, 2, 'Manish', 'Nootan Prakash pandey', 'mathura', '9898978', 'pandey639@gmail.com', 'MCA', 'Desert.jpg'),
(21, 3, 'krishna', 'Shiv Shankar Avatar', 'kosi', '98978877878', 'krishhh@gmail.com', 'MCA', 'Jellyfish.jpg'),
(22, 4, 'priyasharan', 'Ghanshyam ', 'Barsana', '6746534476', 'priya@gmail.com', 'MCA', 'Koala.jpg'),
(23, 5, 'Abhishek', 'Mohan Kumar Arora', 'kosi kalan', '8076675786', 'ansh281403@gmail.com', 'MCA', 'Chrysanthemum.jpg'),
(24, 6, 'Himanshu', 'Pta nahi ', 'Kashganj', '8757698789', 'himanshugunda@gmail.com', 'MCA', 'Koala.jpg'),
(25, 7, 'Omveer', 'eska bhi nahi pta', 'kosi vala', '98978877878', 'omee@gmail.com', 'MCA', 'Desert.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
